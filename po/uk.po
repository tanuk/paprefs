# translation of paprefs.po to Ukrainian
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: paprefs\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2009-09-10 03:48+0200\n"
"PO-Revision-Date: 2009-08-27 09:17+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <translation@linux.org.ua>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 0.3\n"
"Plural-Forms:  nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%"
"10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: ../src/paprefs.desktop.in.h:1 ../src/paprefs.glade.h:17
msgid "PulseAudio Preferences"
msgstr "Налаштування PulseAudio"

#: ../src/paprefs.desktop.in.h:2
msgid "Sound Server Preferences"
msgstr "Налаштування звукового сервера"

#: ../src/paprefs.desktop.in.h:3
msgid "View and modify the configuration of the local sound server"
msgstr "Перегляньте і змініть налаштування локального звукового сервера"

#: ../src/paprefs.glade.h:1
msgid ""
"<i>Apple and AirTunes are trademarks of Apple Inc., registered in the U.S. "
"and other countries.</i>"
msgstr ""
"<i>Apple і AirTunes є торговими марками Apple Inc., зареєстрованими у США та "
"інших країнах світу.</i>"

#: ../src/paprefs.glade.h:2
msgid ""
"Add _virtual output device for simultaneous output on all local sound cards"
msgstr ""
"Додати в_іртуальний пристрій виведення для одночасного виведення на всіх "
"локальних звукових картах"

#: ../src/paprefs.glade.h:3
msgid "Allow other machines on the LAN to _discover local sound devices"
msgstr "Дозволити іншим комп’ютерам з LAN ви_значати локальні звукові пристрої"

#: ../src/paprefs.glade.h:4
msgid "Create separate audio device _for Multicast/RTP"
msgstr "Створити окремий звуковий п_ристрій для трансляції/RTP"

#: ../src/paprefs.glade.h:5
msgid "Create separate audio device for DLNA/UPnP media streaming"
msgstr "Створити окремий звуковий п_ристрій для трансляції DNLA/UPnP"

#: ../src/paprefs.glade.h:6
msgid "Don't _require authentication"
msgstr "Не вимагати р_озпізнавання"

#: ../src/paprefs.glade.h:7
msgid "Enable Multicast/RTP re_ceiver"
msgstr "Увімкнути пр_иймач трансляцій/RTP"

#: ../src/paprefs.glade.h:8
msgid "Enable Multicast/RTP s_ender"
msgstr "Увімкнути п_ередавач трансляцій/RTP"

#: ../src/paprefs.glade.h:9
msgid "Enable _network access to local sound devices"
msgstr "Увімкнути _мережевий доступ до локальних звукових пристроїв"

#: ../src/paprefs.glade.h:10
msgid "Install..."
msgstr "Встановити..."

#: ../src/paprefs.glade.h:11
msgid "Make discoverable Apple A_irTunes sound devices available locally"
msgstr "Зр_обити знайдені звукові пристрої Apple Airtunes доступними локально"

#: ../src/paprefs.glade.h:12
msgid "Make discoverable _PulseAudio network sound devices available locally"
msgstr ""
"_Зробити знайдені мережеві звукові пристрої PulseAudio доступними локально"

#: ../src/paprefs.glade.h:13
msgid "Make local sound devices available as DLNA/_UPnP Media Server"
msgstr ""
"Зробити локальні звукові пристрої доступними як мультимедійний сервер DLNA/"
"_UPnP"

#: ../src/paprefs.glade.h:14
msgid "Multicast/R_TP"
msgstr "Тра_нсляція/RTP"

#: ../src/paprefs.glade.h:15
msgid "Network _Access"
msgstr "Мер_ежевий доступ"

#: ../src/paprefs.glade.h:16
msgid "Network _Server"
msgstr "Мере_жевий сервер"

#: ../src/paprefs.glade.h:18
msgid "Send audio from local _microphone"
msgstr "Надсилати звук з локального м_ікрофона"

#: ../src/paprefs.glade.h:19
msgid "Send audio from local spea_kers"
msgstr "Надсилати звук з локальних г_учномовців"

#: ../src/paprefs.glade.h:20
msgid "Simultaneous _Output"
msgstr "Одно_часне виведення"

#: ../src/paprefs.glade.h:21
msgid "_Loop back audio to local speakers"
msgstr "_Циклічне виведення звуку на локальні гучномовці"
