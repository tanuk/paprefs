# translation of paprefs.master-tx.es.po to Spanish
# Gladys Guerrero Lozano <gguerrer@redhat.com>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: paprefs.master-tx.es\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2009-09-10 03:48+0200\n"
"PO-Revision-Date: 2009-08-25 11:46-0300\n"
"Last-Translator: Domingo Becker <domingobecker@gmail.com>\n"
"Language-Team: Spanish <fedora-trans-es@redhat.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Spanish\n"
"X-Generator: KBabel 1.11.4\n"

#: ../src/paprefs.desktop.in.h:1 ../src/paprefs.glade.h:17
msgid "PulseAudio Preferences"
msgstr "Preferencias de PulseAudio"

#: ../src/paprefs.desktop.in.h:2
msgid "Sound Server Preferences"
msgstr "Preferencias del Servidor de Sonido"

#: ../src/paprefs.desktop.in.h:3
msgid "View and modify the configuration of the local sound server"
msgstr "Ver y modificar la configuración del servidor de sonido local"

#: ../src/paprefs.glade.h:1
msgid ""
"<i>Apple and AirTunes are trademarks of Apple Inc., registered in the U.S. "
"and other countries.</i>"
msgstr ""
"<i>Apple y Airtunes son marcas comerciales de Apple Inc., registradas en los "
"EEUU y en otros países.</i>"

#: ../src/paprefs.glade.h:2
msgid ""
"Add _virtual output device for simultaneous output on all local sound cards"
msgstr ""
"Agregar un dispositivo de salida _virtual para salida simultánea en todas "
"las placas de sonido locales"

#: ../src/paprefs.glade.h:3
msgid "Allow other machines on the LAN to _discover local sound devices"
msgstr ""
"Permitir a otras máquinas en la red _descubrir dispositivos de sonido locales"

#: ../src/paprefs.glade.h:4
msgid "Create separate audio device _for Multicast/RTP"
msgstr "Crear un _dispositivo de audio separado para Multicast/RTP"

#: ../src/paprefs.glade.h:5
msgid "Create separate audio device for DLNA/UPnP media streaming"
msgstr "Crear un dispositivo de audio separado para paso de medios DLNA/UPnP"

#: ../src/paprefs.glade.h:6
msgid "Don't _require authentication"
msgstr "No pedi_r autenticación"

#: ../src/paprefs.glade.h:7
msgid "Enable Multicast/RTP re_ceiver"
msgstr "Habilitar re_ceptor de Multicast/RTP"

#: ../src/paprefs.glade.h:8
msgid "Enable Multicast/RTP s_ender"
msgstr "Habilitar _emisor Multicast/RTP"

#: ../src/paprefs.glade.h:9
msgid "Enable _network access to local sound devices"
msgstr "Habilitar el acceso desde la red a dispositivos de sonido locales"

#: ../src/paprefs.glade.h:10
msgid "Install..."
msgstr "Instalar..."

#: ../src/paprefs.glade.h:11
msgid "Make discoverable Apple A_irTunes sound devices available locally"
msgstr ""
"Hacer detectables los dispositivos Air_tunes de Apple disponibles localmente"

#: ../src/paprefs.glade.h:12
msgid "Make discoverable _PulseAudio network sound devices available locally"
msgstr ""
"Hacer visibles los dispositivos de sonido de red de _PulseAudio disponibles "
"localmente"

#: ../src/paprefs.glade.h:13
msgid "Make local sound devices available as DLNA/_UPnP Media Server"
msgstr ""
"Poner los dispositivos de sonido locales disponibles como Servidor de Medios "
"DLNA/_UPnP"

#: ../src/paprefs.glade.h:14
msgid "Multicast/R_TP"
msgstr "Multicast/R_TP"

#: ../src/paprefs.glade.h:15
msgid "Network _Access"
msgstr "_Acceso a la Red"

#: ../src/paprefs.glade.h:16
msgid "Network _Server"
msgstr "_Servidor de Red"

#: ../src/paprefs.glade.h:18
msgid "Send audio from local _microphone"
msgstr "Enviar audio desde el _micrófono local"

#: ../src/paprefs.glade.h:19
msgid "Send audio from local spea_kers"
msgstr "Enviar audio desde los _parlantes locales"

#: ../src/paprefs.glade.h:20
msgid "Simultaneous _Output"
msgstr "Salida Sim_ultánea"

#: ../src/paprefs.glade.h:21
msgid "_Loop back audio to local speakers"
msgstr "_Devolver el audio a los parlantes locales"

#~ msgid ""
#~ "<span color=\"black\">View and modify the configuration of your local "
#~ "sound server</span>"
#~ msgstr ""
#~ "<span color=\"black\">Ver y modificar la configuración de su servidor de "
#~ "sonido local</span>"

#~ msgid ""
#~ "<span size=\"18000\" color=\"black\"><b>PulseAudio Preferences</b></span>"
#~ msgstr ""
#~ "<span size=\"18000\" color=\"black\"><b>Preferencias de PulseAudio</b></"
#~ "span>"
